package ee.ut.math.tvt;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class BillRowTest {

  private SaleItem item1;
  
  /**
   * Methods with @Before annotations will be invoked before each test is run.
   */
  @Before
  public void setUp() {
    item1 = new SaleItem("Lauaviin", 3.50); 
  }

  @Test
  public void testRowSumWithZeroQuantity() {
    BillRow r = new BillRow(item1, 0);
    
    assertEquals(r.getRowPrice(), 0.0, 0.0001);
  }
  
  @Test
  public void testRowSumWithoutDiscount() {
    BillRow r = new BillRow(item1, 1);
    assertEquals(r.getRowPrice(), 3.5, 0.0001);
  }

  @Test
  public void testRowSumWithDiscount() {
    BillRow r = new BillRow(item1, 2, 10);
    assertEquals(r.getRowPrice(), 6.3, 0.0001);
  }

  @Rule
  public ExpectedException exception = ExpectedException.none();


  @Test
  public void testRowSumWithInvalidDiscount() {
    exception.expect(IllegalArgumentException.class);
    BillRow r = new BillRow(item1, 10, -5);
    r.getRowPrice();
  }
  
  @Test
  public void testRowSumWithNegativeQuantity() {
    exception.expect(IllegalArgumentException.class);
    BillRow r = new BillRow(item1, -10, 5);
    r.getRowPrice();
  }

  @Test
  public void testRowSumWithNegativePrice() {
    exception.expect(IllegalArgumentException.class);
    SaleItem item2 = new SaleItem("SI2", -3);
    BillRow r = new BillRow(item2, 10, 5);
    r.getRowPrice();
  }
  
}
