package ee.ut.math.tvt;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class BillTest {

    // Bill row costs (reused)
    private static double cost1 = 452.12;
    private static double cost2 = 65.22;
    private static double cost3 = 102.0;
    private static double totalSum = 619.34;

    private SaleItem si1;
    private SaleItem si2;
    private SaleItem si3;
    private BillRow billRow1;
    private BillRow billRow2;
    private BillRow billRow3;


    /**
     * Methods with @Before annotations will be invoked before each test is run.
     */
    @Before
    public void setUp() {
        si1 = new SaleItem("si1", cost1);
        si2 = new SaleItem("si2", cost2);
        si3 = new SaleItem("si3", cost3);
        billRow1 = new BillRow(si1, 1);
        billRow2 = new BillRow(si2, 1);
        billRow3 = new BillRow(si3, 1);
    }

    @Test
    public void testBillWithNoRows() {
        Bill bill = new Bill();
        double totalSum = bill.getTotalSum();
        assertEquals(totalSum, 0.0, 0.0001);
    }


    @Test
    public void testBillWithOneRow() {
        Bill bill = new Bill();
        bill.addBillRow(billRow1);
        double totalSum = bill.getTotalSum();
        assertEquals(totalSum, 452.12, 0.0001);
    }

    
    @Test
    public void testBillWithManyRows() {
        Bill bill = new Bill();
        bill.addBillRow(billRow1);
        bill.addBillRow(billRow2);
        bill.addBillRow(billRow3);
        double totalSum = bill.getTotalSum();
        assertEquals(totalSum, 619.34, 0.0001);
    }
    
}
